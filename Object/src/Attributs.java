import monPackage.Atmosphere;
import monPackage.Planetes;
import monPackage.Vaisseau;

public class Attributs {

    public static void main(String[] args) {
        Planetes terre = new Planetes();
        terre.nom="Terre";
        terre.matiere="tellurique";
        terre.diametre=12756;
        terre.angle=360;
        System.out.println("La planète "+terre.nom+" est une planète "+terre.matiere+" dont le diamètre est de "+terre.diametre+" Km.");
        Planetes mercure = new Planetes();
        mercure.nom="Mercure";
        mercure.matiere="tellurique";
        mercure.diametre=4880;
        System.out.println("La planète "+mercure.nom+" est une planète "+mercure.matiere+" dont le diamètre est de "+mercure.diametre+" Km.");
        // ce qui s'affiche correspond à l'emplacement de l'objet "terre".
        System.out.println(terre);
        Planetes x = new Planetes();
        //Lorsqu'on ne définit rien et qu'on cherche à faire afficher l'objet, par défaut l'objet en String est null et en int est 0.
        System.out.println("La planète "+x.nom+" est une planète "+x.matiere+" dont le diamètre est de "+x.diametre+" Km.");
        x.nom="xxx";
        x.matiere="gazeuse";
        x.diametre=1000;
        System.out.println("La planète "+x.nom+" est une planète "+x.matiere+" dont le diamètre est de "+x.diametre+" Km.");
        mercure.revolution(mercure.nom);
        mercure.rotation(mercure.nom);
        mercure.rotationsansargument();
        terre.revolution(terre.nom);
        terre.rotation(terre.nom);
        System.out.println(terre.rotationangle(terre.angle));
        System.out.println(terre.rotationangle(360));
        mercure.accueillirvaisseau(8);
        mercure.accueillirvaisseau("fregate");
        Atmosphere atmosphereMercure= new Atmosphere();
        atmosphereMercure.txhydrogene=72.3f;
        mercure.atmosphere=atmosphereMercure;
        System.out.println(mercure.atmosphere.txhydrogene);
        System.out.println(atmosphereMercure.txhydrogene);
        Vaisseau vaisseau1= new Vaisseau();
        vaisseau1.type = "Fregate";
        vaisseau1.nbpassager = 9;






    }
}
