package monPackage;

public class Spectateur {
    String nom;
    int age;

    public Spectateur (String n, int a){
        this.nom= n;
        this.age=a;
    }

    public String toStrin()
    {
        return String.format("Le nom est %s et il a %d ans",this.nom, this.age);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {
        Spectateur spectateur1 = new Spectateur("Marie",20);
        System.out.println(spectateur1);
        System.out.println(spectateur1.getNom());
        System.out.println(spectateur1.getAge());
    }
}
