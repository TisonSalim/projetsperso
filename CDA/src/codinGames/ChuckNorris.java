package codinGames;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class ChuckNorris {

    public static void main(String args[]) {
        // Scanner in = new Scanner(System.in);
        // String MESSAGE = in.nextLine();

        // Write an action using System.out.println()
        String MESSAGE = "C";
        String answer = "";

        for (int i = 0; i < MESSAGE.length(); i++) {
            int carac = MESSAGE.charAt(i);

            String bin = Integer.toBinaryString(carac);
            for (int j = 0; j <= bin.length();) {

                if (bin.charAt(j) == '1') {
                    answer = answer + "0 ";
                    while (bin.charAt(j) == '1') {

                        answer = answer + "0";
                        //if(bin.charAt(j++1))
                            j++;
                    }
                } else {
                    answer = answer + "00 ";
                    while (bin.charAt(j) == '0') {
                        j++;
                        answer = answer + "0";
                    }
                }
                answer= answer+" ";
            }
        }

        // To debug: System.err.println("Debug messages...");
        System.out.println(answer);
    }
}
