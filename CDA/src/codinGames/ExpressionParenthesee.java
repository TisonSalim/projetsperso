package codinGames;

import java.util.*;
import java.io.*;
import java.math.*;
/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class ExpressionParenthesee {
    public static boolean compterOccurrences(String maChaine, char rechercheU, char rechercheUne) {
        int nb = 0;
        for (int i = 0; i < maChaine.length(); i++) {
            if (maChaine.charAt(i) == rechercheU) {
                nb++;
            } else if (maChaine.charAt(i) == rechercheUne) {
                nb--;
                if (nb < 0) {
                    return false;
                }
            }
        }
        if (!(nb==0)) {
            return false;
        }
        else return true;
    }
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String expression = in.next();
        compterOccurrences(expression, '(', ')');
        compterOccurrences(expression, '[', ']');
        compterOccurrences(expression, '{', '}');
        if ((compterOccurrences(expression, '(', ')') == false) || (compterOccurrences(expression, '[', ']') == false) || (compterOccurrences(expression, '{', '}') == false)) {
            System.out.println(false);
        }
        else {
            System.out.println(true);
        }
    }
}