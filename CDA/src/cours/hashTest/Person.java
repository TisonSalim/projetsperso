package cours.hashTest;

import java.util.Objects;

public class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equals(person.name);
    }
    //Si le nom est le même le hashCode est le même pour les 2 objets donc si 2 person s'appellent "Daniel" il n'y aura que 1 Daniel dans le hashSet
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
