package cours.hashTest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Test {

    public static void main(String[] args) {
        Person daniel = new Person("Daniel", 32);
        Person doubleDaniel = new Person("Daniel", 32);
        Person mia = new Person("Mia", 45);
        Person bob = new Person("Bob", 40);
        Person jos = new Person("Jos", 23);

        Set<Person>personSet = new HashSet<>();
        personSet.add(daniel);
        personSet.add(daniel);
        personSet.add(mia);
        personSet.add(bob);
        personSet.add(jos);

        List<Person>personList = new ArrayList<>();
        personList.add(daniel);
        personList.add(daniel);
        personList.add(mia);
        personList.add(bob);
        personList.add(jos);
        //Ordre différent et pas de doublon
        System.out.println("HashSet : ");
        for (Person personnes:personSet
             ) {
            System.out.println("Nom: " + personnes.getName()+ "   | Age: "+personnes.getAge());
        }
        System.out.println("\nArrayList : ");
        for (Person personnes:personList
             ) {
            System.out.println("Nom: " + personnes.getName()+ "   | Age: "+personnes.getAge());
        }
        //Chaque chose à un hashchode (integer, getname, objet, tout !) et si les caractéristiques sont les mêmes le hashcode sera le même
        System.out.println(daniel.hashCode());
        System.out.println(doubleDaniel.hashCode());
        System.out.println(daniel.getName().hashCode());
        System.out.println(doubleDaniel.getName().hashCode());
        System.out.println(new String("Daniel").hashCode());


    }

}
