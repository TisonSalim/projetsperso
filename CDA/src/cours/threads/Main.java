package cours.threads;

public class Main extends Thread {

    public static void main(String[] args) throws InterruptedException {
        Proc1 p1 = new Proc1();
        p1.setName("Salim");
        p1.start();
        Proc2 p2 = new Proc2();
        p2.start();

        while (true){
            System.out.println("Je suis le Main");
            sleep(1000);
        }

    }

}
