package cours.sockets.main;

import java.io.BufferedInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
public class Client {
    public static void main(String[] args) throws IOException {
        Socket sc = new Socket("10.115.57.18", 8989);
        PrintWriter writer = new PrintWriter(sc.getOutputStream());
        BufferedInputStream reader = new BufferedInputStream(sc.getInputStream());
        writer.write("Je suis le client");
        writer.flush();
        String lu = read(reader);
        System.out.println("le serveur à dit" + lu);
        reader.close();
        writer.close();
        sc.close();
    }
    // La méthode que nous utilisons pour lire les réponses
    private static String read(FilterInputStream reader) throws IOException {
        String response = "";
        int stream;
        byte[] b = new byte[4096];
        stream = reader.read(b);
        response = new String(b, 0, stream);
        return response;
    }
}
