package cours.sockets.socketsClientServeurTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServeur {
    public static void main(String[] args) throws IOException {

//
        int port = 1000;
        ServerSocket sa = new ServerSocket(port);
        System.out.println("Serveur actif sur le port " + port);
        while (true) {
            Socket soc = sa.accept();
            InputStream flux = soc.getInputStream();
            BufferedReader entree = new BufferedReader(new InputStreamReader(flux));
            String message = entree.readLine();
            System.out.println("Message reçu sur le serveur " + message);
        }
    }
}
