package cours.sockets.socketsClientServeurTest;


import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class ClientSocketTest {


    public static void main(String[] args) throws IOException {
        String hote = "82.127.54.187";
        int port = 1000;
        Socket soc = new Socket(hote, port);
        OutputStream flux = soc.getOutputStream();
        OutputStreamWriter sortie = new OutputStreamWriter(flux);
        sortie.write("Message envoyé au serveur");
        sortie.flush();
    }

}