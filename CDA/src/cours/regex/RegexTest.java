package cours.regex;

import java.util.Scanner;
public class RegexTest {

        public static String id;
        public static String age;
        public static String date;
        public static String email;
        public static String prix;
        public static boolean isValidId(String id) {
            if (id == null) {
                return false;
            }
            String regExp = "^[A-Z][a-z]+ [A-Z][a-z]+$";
            return id.matches(regExp);
        }
        public static boolean isValidAge(String age) {
            if (age == null) {
                return false;
            }
            String regExp = "^[1-9][0-9]{0,2}$";
            return age.matches(regExp);
        }
        public static boolean isValidDate(String date) {
            if (date == null) {
                return false;
            }
            String regExp = "^[0-9]{2}/[0-9]{2}/[0-9]{2}$";
            return date.matches(regExp);
        }
        public static boolean isValidEmail(String email) {
            if (email == null) {
                return false;
            }
            String regExp = "^[\\w.-]+@[\\w.-]+\\.[a-z]{2,}$";
            return email.matches(regExp);
        }
        public static boolean isValidPrix(String prix) {
            if (prix == null) {
                return false;
            }
            String regExp = "";
            return prix.matches(regExp);
        }
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            while (!isValidId(id)) {
                System.out.println("Saisir votre nom et prénom :");
                id = sc.nextLine();
            }
            while (!isValidAge(age)) {
                System.out.println("Veuillez saisir votre âge :");
                age = sc.next();
            }
            while (!isValidDate(date)) {
                System.out.println("Veuillez saisir la date de naissance :");
                date = sc.next();
            }
            while (!isValidEmail(email)) {
                System.out.println("Veuillez saisir votre email :");
                email = sc.next();
            }
            while (!isValidPrix(prix)) {
                System.out.println("Veuillez saisir le prix du produit :");
                String prix = sc.next();
            }
        }
    }
