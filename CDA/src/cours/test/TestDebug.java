package cours.test;

public class TestDebug {
    public static void main(String[] args) {
        int a=1;
        int b=3;
        int c;
        c= a+b;
        String s = "Salut";
        System.out.format("La valeur de c est : %d \n et la valeur de s est : %s %n",c ,s);
        System.out.printf("La valeur de c est : %d  \n et la valeur de s est : %s %n",c ,s);
        System.out.println("La valeur de c est : " + c + "\n" + "et la valeur de s est : "+ s);
    }
}
