package cours.tpHeritage;

public class Test { public static void main(String[] args) {

    Livre l1 = new Livre("Le petit prince","St Exupéry",10.40, 50) ;
    Livre l2 = new Livre("Contes","Grimm",14.40,254) ; l1.Afficher() ;
    //acheter
    l1.Acheter("moi") ; l1.Afficher() ; l1.prix = 0.0 ; l2.Acheter("lui") ;
    l2.Afficher();

    BD b1 = new BD("Lucky Luke","Morris",10.40, 45, true);
    BD b2 = new BD("Tintin","Herge",200.40, 45, false) ;
    b1.Acheter("moi");
    b1.Afficher() ;
    b2.Afficher() ;

    Album a1 = new Album("Dora","Dora", 300, 3) ;
    a1.Afficher() ; a1.Colorie(23) ;
    a1.Acheter("moi");
    a1.Colorie(23) ;
}
}
