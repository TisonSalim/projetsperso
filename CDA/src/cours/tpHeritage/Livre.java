package cours.tpHeritage;

class Livre {
    //les attributs protected
    //erreur string
    String titre, auteur, proprietaire;
    protected int nb_page;
    double prix;

    // les méthodes
    public Livre(String t, String a, double p, int nb) {
        titre = t;
        auteur = a;
        prix = p;
        proprietaire = "";
        nb_page = nb;
    }


    public void Afficher() {


        System.out.println("Titre : " + titre);
        System.out.println("Auteur : " + auteur);
        System.out.println("Prix : " + prix);
        System.out.println("Nombre de pages : " + nb_page);
        if (this.Est_neuf()) {
            System.out.println("Aucune proprietaire");
        } else {
            System.out.println("Proprietaire: " + proprietaire);
        }
        System.out.println();
    }

    public boolean Est_neuf() {
        if (proprietaire == "") return true;
        else return false;
    }

    public void Acheter(String nom) {
        proprietaire = nom;
    }
}

//extend
class BD extends Livre {
    private boolean encouleur;

    public BD(String t, String a, double p, int nb, boolean c) {
        super(t, a, p, nb);
        encouleur = c;
    }
}

class Album extends Livre {
    boolean page_coloriee[];

    public Album(String t, String a, double p, int n) {
        super(t, a, p, n);
        page_coloriee = new boolean[n];
        int i;
        for (i = 0; i < 100; i++)
            page_coloriee[i] = false;
    }

    public void Colorie(int num_page) {
        if ((page_coloriee[num_page] == false) && !Est_neuf()) {
            page_coloriee[num_page] = true;
        } else {
            System.out.println("page deja coloriee");
        }
    }
}



