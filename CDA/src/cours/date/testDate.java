package cours.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class testDate {

    public static void main(String[] args) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd/MM/yy");
        Date d = new Date();
        System.out.println(format.format(d));
        String maDate = "12/12/1995";
        d = format.parse(maDate);
        System.out.println("Nous sommes le " +format.format(d));
    }
}
