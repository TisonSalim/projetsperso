package cours.exercices.http;

public interface HttpBody {

    public String getContent();

    public byte[] getBinayContent();

    public String getContentType();

}
