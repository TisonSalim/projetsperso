package cours.exercices.http;

import java.util.Map;

public class HttpRequest {

    private String methode;
    private String protocol;
    private String versionProtocol;
    private String url;

    private Map<String, String> Headers;
    private HttpBody body;

    public static String format() {
        StringBuilder stringBuilder = new StringBuilder();
        // request to string
        return "";

    }

    public static HttpRequest parse(String requestString) {
        HttpRequest request = new HttpRequest();
        // ici parser le string afin de remplir la reponse

        return request;

    }

    public HttpRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getMethode() {
        return methode;
    }

    public void setMethode(String methode) {
        this.methode = methode;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getVersionProtocol() {
        return versionProtocol;
    }

    public void setVersionProtocol(String versionProtocol) {
        this.versionProtocol = versionProtocol;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHeaders() {
        return Headers;
    }

    public void setHeaders(Map<String, String> headers) {
        Headers = headers;
    }

    public HttpBody getBody() {
        return body;
    }

    public void setBody(HttpBody body) {
        this.body = body;
    }

}
