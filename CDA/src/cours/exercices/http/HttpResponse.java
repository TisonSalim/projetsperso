package cours.exercices.http;

import java.util.Map;

public class HttpResponse {

    private Integer code;
    private String message;
    private String protocol;
    private String versionProtocol;


    private Map<String, String> Headers;
    private HttpBody body;

    public static String format() {
        StringBuilder stringBuilder = new StringBuilder();
        // request to string
        return "";

    }

    public static HttpResponse parse(String requestString) {
        HttpResponse request = new HttpResponse();
        // ici parser le string afin de remplir la reponse

        return request;

    }

    public HttpResponse() {
        super();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getVersionProtocol() {
        return versionProtocol;
    }

    public void setVersionProtocol(String versionProtocol) {
        this.versionProtocol = versionProtocol;
    }

    public Map<String, String> getHeaders() {
        return Headers;
    }

    public void setHeaders(Map<String, String> headers) {
        Headers = headers;
    }

    public HttpBody getBody() {
        return body;
    }

    public void setBody(HttpBody body) {
        this.body = body;
    }



}
