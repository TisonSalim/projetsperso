package cours.exercices.scannerFichier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TestScanner {
    public static void main(String[] args) throws FileNotFoundException {
        String nomdufichier = "C:/Users/selim/Desktop/doc1.csv";
        File file = new File(nomdufichier);
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()){
            String line = scanner.nextLine();
            System.out.println(line);
        }

        scanner.close();

    }


}
