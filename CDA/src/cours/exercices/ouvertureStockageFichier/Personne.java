package cours.exercices.ouvertureStockageFichier;

import java.util.Date;

public class Personne {

    private int id;
    private String nom;
    private String prenom;
    private Date dateNaissance;

    public Personne() {
        super();
    }


    public Personne(int id, String nom, String prenom, Date dateNaissance) {
        super();
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;

    }


    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getNom() {

        return nom;
    }

    public void setNom(String nom) {

        this.nom = nom;
    }

    public String getPrenom() {

        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {

        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {

        this.dateNaissance = dateNaissance;
    }

    @Override
    public String toString() {
        return "Personne{" +
                "id= " + id +
                ", nom=' " + nom + '\'' +
                ", prenom=' " + prenom + '\'' +
                ", date de naissance= " + dateNaissance +
                '}';
    }
}
