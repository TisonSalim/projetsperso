package cours.exercices.ouvertureStockageFichier;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        List<Personne> personnes = lirePersonnesDepuisCSV("C:/Users/selim/Desktop/personnes.csv");
        for (Personne p : personnes
        ) {
            System.out.println(p);
        }
    }

    private static List<Personne> lirePersonnesDepuisCSV(String filename) {
        List<Personne> personnes = new ArrayList<>();
        Path pathToFile = Paths.get(filename);

        try (BufferedReader bufferedReader = Files.newBufferedReader(pathToFile, StandardCharsets.UTF_8)) {

            String line = bufferedReader.readLine();

            while (line != null) {
                String[] attributs = line.split(",");

                Personne personne = nouvellePersonne(attributs);

                personnes.add(personne);

                line = bufferedReader.readLine();
            }

        } catch (IOException | ParseException e) {
            System.out.println("Erreur");
        }
        return personnes;
    }

    private static Personne nouvellePersonne(String[] metadata) throws ParseException {
        int id = Integer.parseInt(metadata[0]);
        String nom = metadata[1];
        String prenom = metadata[2];
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateNaissance = dateFormat.parse(metadata[3]);

         return new Personne(id, nom, prenom, dateNaissance);
    }
}
