package cours.myShell;

import java.io.IOException;
import java.nio.file.*;

public class TestMyShell {
    public static void main(String[] args) {

        //On récupère maintenant la liste des répertoires dans une collection typée
//Via l'objet FileSystem qui représente le système de fichier de l'OS hébergeant la JVM
       Iterable<Path> roots = FileSystems.getDefault().getRootDirectories();

//Maintenant, il ne nous reste plus qu'à parcourir
       for (Path chemin : roots) {
            System.out.println(chemin);
            //Pour lister un répertoire, il faut utiliser l'objet DirectoryStream
            //L'objet Files permet de créer ce type d'objet afin de pouvoir l'utiliser
           // try (DirectoryStream<Path> listing = Files.newDirectoryStream(chemin, "*.txt")){
           try (DirectoryStream<Path> listing = Files.newDirectoryStream(chemin))  {

                int i = 0;
                for (Path nom : listing) {
                    System.out.print("\t\t" + ((Files.isDirectory(nom)) ? nom + "/" : nom));
                    i++;
                    if (i % 4 == 0) System.out.println("\n");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
