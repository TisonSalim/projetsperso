/* eslint-disable */
import React, {FunctionComponent, ReactDOM, useEffect, useState} from 'react';
import Product from "../models/product";
import ProductService from "../services/product-service";
import PRODUCTS from "../models/mock-product";
import product from "../models/product";
import Drink from "../models/drink";
import DRINKS from "../models/mock-drink";


const MenuCard: FunctionComponent = () => {
    const [products, setProducts] = useState<Product[]>(PRODUCTS);
    const [drinks, setDrinks] = useState<Drink[]>(DRINKS);



    return (
        <div>
    {/* <div className="scrollmenu">
        <a href="#hamburgers">Hamburgers</a>
        <a href="#accompagnements">Accompagnements</a>
        <a href="#supplements">Suppléments</a>
        <a href="#desserts">Desserts</a>
        <a href="#support">Support</a>
        <a href="#blog">Blog</a>
        <a href="#tools">Tools</a>  
</div> */}
        <div className="content  "  >
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-6 text-center  ">
                        <div className="page-section">
                            <h1 className="page-title mt-3 mb-5">Carte des menus</h1>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb40" >
                        <div className="menu-block"  >
                            <section data-spy>
                            <h3 className="menu-title"  id="hamburgers">Hamburgers</h3>
                            {products.map(product => (
                                <div className="menu-content" key={product.name}>
                                    <div className="row">
                                        <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div className="dish-img"><a href="#"><img
                                                src={product.littlepicture} alt="" className="img-rounded-corners"/></a>
                                            </div>
                                        </div>
                                        <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                            <div className="dish-content">
                                                <h5 className="dish-title"><a href="#">{product.name}</a></h5>
                                                <span className="dish-meta"> Onion  /  Tomato</span>
                                                <div className="dish-price">
                                                    <p>{product.pricealacard} € seul/{product.priceinmenu}€ en menu</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>))}
                        </section>
                    </div>
                </div>
                <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb40">
                    <div className="menu-block">
                        <section data-spy>

                            <h3 className="menu-title" id="accompagnements" >Accompagnements</h3>
                            <div className="menu-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div className="dish-img"><a href="#"><img
                                            src="../assets/hamburgers/mozzatenders.jpeg " alt=""
                                            className="big-img-rounded-corners"/></a>
                                        </div>
                                    </div>
                                    <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div className="dish-content">
                                            <h5 className="dish-title"><a href="#">Tenders nature ou épicés</a></h5>
                                            <span className="dish-meta">Onion  /  Tomato</span>
                                            <div className="dish-price">
                                                <p>$10</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="menu-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div className="dish-img"><a href="#"><img
                                            src="../assets/hamburgers/mozzatenders.jpeg " alt=""
                                            className="big-img-rounded-corners"/></a>
                                        </div>
                                    </div>
                                    <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div className="dish-content">
                                            <h5 className="dish-title"><a href="#">Mozza Stick</a></h5>
                                            <span className="dish-meta">Onion  /  Tomato</span>
                                            <div className="dish-price">
                                                <p>$10</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="menu-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div className="dish-img"><a href="#"><img
                                            src="../assets/hamburgers/frites.jpeg " alt=""
                                            className="big-img-rounded-corners"/></a>
                                        </div>
                                    </div>
                                    <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div className="dish-content">
                                            <h5 className="dish-title"><a href="#">Frites</a></h5>
                                            <span className="dish-meta">Onion  /  Tomato</span>
                                            <div className="dish-price">
                                                <p>2.50€</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section >
                        </div>
                        <div className="menu-block"  >
                            <h3 className="menu-title"  id="supplements">Suppléments</h3>
                            <div className="menu-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div className="dish-img"><a href="#"><img
                                            src="../assets/hamburgers/bacon.jpg " alt=""
                                            className="big-img-rounded-corners"/></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div className="dish-content">
                                        <h5 className="dish-title"><a href="#">Bacon</a></h5>
                                        <span className="dish-meta">Onion  /  Tomato</span>
                                        <div className="dish-price">
                                            <p>$10</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="menu-content">
                            <div className="row">
                                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div className="dish-img"><a href="#"><img
                                        src="../assets/hamburgers/sauce.jpg " alt=""
                                        className="big-img-rounded-corners"/></a>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div className="dish-content">
                                        <h5 className="dish-title"><a href="#">Sauce</a></h5>
                                        <span className="dish-meta">Onion  /  Tomato</span>
                                        <div className="dish-price">
                                            <p>$10</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="menu-content">
                            <div className="row">
                                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div className="dish-img"><a href="#"><img
                                        src="../assets/hamburgers/cheddar.jpg " alt=""
                                        className="big-img-rounded-corners"/></a>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div className="dish-content">
                                        <h5 className="dish-title"><a href="#">Cheddar</a></h5>
                                        <span className="dish-meta">Onion  /  Tomato</span>
                                        <div className="dish-price">
                                            <p>$10</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb40">
                        <div className="menu-block"  >
                            <h3 className="menu-title" id="desserts">Desserts</h3>
                            <div className="menu-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div className="dish-img"><a href="#"><img
                                            src="../assets/hamburgers/minitiramisu.jpeg " alt=""
                                            className="img-rounded-corners"/></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div className="dish-content">
                                        <h5 className="dish-title"><a href="#">Tiramisu Spéculoos ou Oréo</a></h5>
                                        <span
                                            className="dish-meta"> Mascarpone / Biscuits  </span>
                                        <div className="dish-price">
                                            <p>$8</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="menu-content mb-3">
                            <div className="row">
                                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div className="dish-img"><a href="#"><img
                                        src="../assets/hamburgers/caferichard.jpg " alt=""
                                        className="img-rounded-corners"/></a>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div className="dish-content">

                                        <h5 className="dish-title"><a href="#">Café</a></h5>
                                        <span className="dish-meta">   </span>
                                        <div className="dish-price">
                                            <p>1.5€</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="menu-content">
                            <div className="row">
                                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div className="dish-img"><a href="#"><img
                                        src="../assets/hamburgers/the.jpg " alt="" className="img-rounded-corners"/></a>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div className="dish-content">    <span
                                        className="dish-meta">   </span>
                                        <h5 className="dish-title"><a href="#">Thé</a></h5>
                                        <div className="dish-price">
                                            <p>1.5€</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        <div className="menu-block">
                            <h3 className="menu-title">Boissons</h3>
                            {drinks.map(drink =>(
                            <div className="menu-content" key={drink.name}>
                                <div className="row">
                                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div className="dish-img"><a href="#"><img
                                            src={drink.picture} alt=""
                                            className="img-rounded-corners"/></a>
                                        </div>
                                    </div>
                                    <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div className="dish-content">
                                            <span
                                                className="dish-meta">{drink.name} </span>
                                            <div className="dish-price">
                                                <p>{drink.price}€</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>

        </div>
    );
}


export default MenuCard;
