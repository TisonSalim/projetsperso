import React, {FunctionComponent, useState, useEffect, Fragment} from 'react';
import ProductCard from "../component/product-card";
import Product from "../models/product";
import ProductService from "../services/product-service";


const ProductList: FunctionComponent = () => {
    const [products, setProducts] = useState<Product[]>([]);
    const [modalProduct, setModalProduct] = useState<Product>(       {id: 0,
        name:'...',
        menupicture : '..',
        bigpicture: '..',
        littlepicture:  '..',
        epices: '',
        ingredients: ['Normal'],
        pricealacard:  0,
        priceinmenu: 0});
    const [details, setDetails] = useState<boolean>(false);
    const [css, setCss] = useState("");

    useEffect(() => {
        ProductService.getProducts().then(products => setProducts(products))
    }, [modalProduct]);

    const showProduct = (id: number, name: string, menupicture: string, bigpicture: string, littlepicture: string, epices: string, ingredients: Array<string>, pricealacard: number, priceinmenu: number) =>
    {setDetails(true);
    setModalProduct( {id, name, menupicture, bigpicture, littlepicture, epices, ingredients, pricealacard, priceinmenu})
    }


    const closeModal = () => {
        setCss("slideDown");
        setTimeout(() => {
            setCss("");
            setDetails(false);
        }, 500);
    }

    return (

        <div className="container ">
            <div className="row  justify-content-center    ">
                {products.map(product => (
                    <ProductCard
                        key={product.id}
                        product={product}
                        handleChange={showProduct}
                    />  ))}
            </div >
            {details && <div className={`modaldetails modalsmaller    ${css}`}>
                <svg
                    onClick={closeModal}
                    className="close flex d-flex justify-content-end"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 -5 32 32"
                    fill="black"
                    width="32px"
                    height="32px"
                >
                    <path d="M0 0h24v24H0z" fill="none"/>
                    <path
                        d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                </svg>
            <img src={modalProduct.bigpicture} alt="Photo" className="mw-100 corners"/>
                                <ul className="list-group list-group-flush"   >
                                    <li className="list-group-item">Ingrédients :
                                        {modalProduct.ingredients.map(ingredient =>
                                            <p className="mt-2">{ingredient}</p>  )}
                                    </li>
                                    <li className="list-group-item">Prix seul:
                                        <p className="mt-2">{modalProduct.pricealacard} euros.</p>
                                    </li>
                                    <li className="list-group-item">Prix en menu:
                                        <p className="mt-2">{modalProduct.priceinmenu} euros.</p>
                                    </li>
                                </ul>
                            </div>

                            //    <DetailsModal product={product} details={details} handleClose={closeModal}/>//
                        }
        </div>
    );
};

export default ProductList;
