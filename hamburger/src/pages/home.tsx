import React, {FunctionComponent} from 'react';
import {Link, Switch} from 'react-router-dom';
import Header from "../component/header";


const Home: FunctionComponent = () => {
    // <div className="d-flex justify-content-center mt-5 ">
    //     <Link type="button" className="button button1" to="/produits">Hamburgers</Link>
    //     <Link type="button" className="button button1" to="/produits">Accompagnements</Link>
    //     <Link type="button" className="button button1" to="/produits">Desserts et boissons</Link>
    // </div>

    return (
        <div >
            <div className="d-flex justify-content-center">
                <img className="" src="../assets/hamburgers/logo.svg" alt="Accueil"/>
            </div>
            <Link className="h1-responsive  d-flex justify-content-center darkgray" to="/carte">Carte des menus</Link>
        </div>
    );
}


export default Home;
