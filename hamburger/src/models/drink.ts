export default class Drink {
    id: number;
    name: string;
    price: number;
    picture: string;

    constructor(id: number, name: string= "", price: number=1.5, picture: string="../assets/boissons/pcf.png ") {
        this.id = id;
        this.name = name;
        this.price = price;
        this.picture = picture;
    }


}
