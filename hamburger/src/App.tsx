import React, {FunctionComponent} from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import PageNotFound from "./pages/page-not-found";
import "./App.css"
import ProductList from "./pages/product-list";
import Header from "./component/header";
import MenuCard from "./pages/menucard";
import HorizontalScroll from "./pages/menu";

const App: FunctionComponent = () => {
    return (
        <Router>
            <Header/>
            <div className="">
                <Switch>
                    <Route exact path="/test" component={HorizontalScroll}/>
                    <Route exact path="/produits" component={ProductList}/>
                    <Route exact path="/carte" component={MenuCard}/>
                    <Route component={PageNotFound}/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;


