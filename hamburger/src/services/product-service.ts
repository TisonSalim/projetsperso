import PRODUCTS from "../models/mock-product";
import Product from "../models/product";


export default class ProductService {

    static products: Product[] = PRODUCTS;

    static getProducts(): Promise<Product[]> {
        return new Promise(resolve => {
            resolve(this.products);
        });
    }

    static getProduct(id: number): Promise<Product | null> {
        return new Promise(resolve => {
            resolve(this.products.find(product => product.id === id));
        });
    }

    static isEmpty(data: Object): boolean {
        return Object.keys(data).length === 0;
    }

    static handleError(error: Error): void {
        console.error(error);
    }
}
