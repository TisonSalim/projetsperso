import {FunctionComponent} from "react";
import React from "react";
import {NavLink} from "react-router-dom";


const Header : FunctionComponent = ()=> {


    return (
        
<div className="scrollmenu">
        <NavLink to="/produits" activeClassName="current">Hamburgers</NavLink>
        <NavLink to="/carte" activeClassName="current">Accompagnements</NavLink>
        <NavLink to="/test" activeClassName="current">Suppléments</NavLink>
        <NavLink to="/" activeClassName="current">Desserts</NavLink>
</div>
      
    );
}


export default Header;
