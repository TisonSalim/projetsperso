package cda;

import java.nio.file.Path;
import java.nio.file.Paths;


public class FichiersDriver {
    public static void main(String[] args) {
        Path path = Paths.get("salim/desktop/doc1.txt");

        System.out.println(path.getFileName());
        for (Path name : path) {
            System.out.println(name);
        }
    }
}
