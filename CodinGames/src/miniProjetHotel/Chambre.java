package miniProjetHotel;

import java.util.*;

public class Chambre {
    private Boolean statut;
    private int id;
    private static int cpt;


    public Chambre() {
        Random status = new Random();
        this.statut = status.nextBoolean();
        cpt++;
        this.id = cpt;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }


    String chambrestatus;

    @Override
    public String toString() {
        if (this.statut == false) {
            chambrestatus = "Chambre libre.";
        } else {
            chambrestatus = "Chambre occupée.";
        }
        return "Chambre " +
                "numéro '" + this.id + '\'' +
                ", statut=" + chambrestatus + '\n';
    }
}
