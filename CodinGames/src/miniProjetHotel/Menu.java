package miniProjetHotel;

import java.util.List;

public class Menu {
    private static List<Chambre>chambreList;


    public Menu(List<Chambre>chambreList) {
        this.chambreList=chambreList;
    }


    public List<Chambre> getChambreList() {
        return chambreList;
    }

    public void setChambreList(List<Chambre> chambreList) {
        this.chambreList = chambreList;
    }
    


    @Override
    public String toString() {
        return  "A-\tAfficher l'état de l'hotel \n" +
                "B-\tAfficher le nombre de chambres réservées\n" +
                "C-\tAfficher le nombre de chambres libres\n" +
                "D-\tAfficher le numéro de la première chambre vide\n" +
                "E-\tAfficher le numéro de la dernière chambre vide\n" +
                "F-\tRéserver une chambre (Le programme doit réserver la première chambre vide)\n" +
                "G-\tLibérer une chambre (Le programme doit libérer la dernière chambre occupée)\n" +
                "Q- Quitter \n";



    }
}
