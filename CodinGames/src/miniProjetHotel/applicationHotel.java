package miniProjetHotel;

import java.util.*;


public class applicationHotel {
    public static void main(String[] args) {
        Scanner scmenu=new Scanner(System.in);
        String reponse;
        List<Chambre> chambreList=new ArrayList<Chambre>();
        for (int i=1; i<21; i++) {
            chambreList.add(new Chambre());
        }

        Hotel hotel1=new Hotel(" George V ",chambreList);
        Menu menuHotel=new Menu(chambreList);
        do {
            System.out.println(menuHotel);
            System.out.println(chambreList);
            reponse = scmenu.next();
            //reponse = reponse.toUpperCase();
            switch (reponse) {
                case "A":
                    System.out.println("A- Afficher l'état de l'hotel.");
                    hotel1.hotelCompletOuPas(chambreList);
                    break;
                case "B" :
                    System.out.println("B- Afficher le nombre de chambres réservées");
                    System.out.println(hotel1.chambresReservées(chambreList));
                    break;
                case "C" :
                    System.out.println("C- Afficher le nombre de chambres libres");
                    System.out.println(hotel1.chambresLibres(chambreList));
                    break;
                case "D" :
                    System.out.println("D- Afficher le numéro de la première chambre vide");
                    System.out.println(hotel1.premiereChambreLibre(chambreList));
                    break;
                case "E" :
                    System.out.println("E- Afficher le numéro de la dernière chambre vide");
                    System.out.println(chambreList.lastIndexOf("chambre occupée"));
                    break;
                case "F" :
                    System.out.println("F- Réserver une chambre (Le programme doit réserver la première chambre vide)");
                    break;
                case "G" :
                    System.out.println("G Libérer une chambre (Le programme doit libérer la dernière chambre occupée)");
                    break;
                case "Q" :
                    System.exit(0);



            }


        }while (reponse!="Q");
    }
}
