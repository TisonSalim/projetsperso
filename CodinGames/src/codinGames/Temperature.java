package codinGames;

import java.util.*;

import static java.lang.Math.abs;

public class Temperature {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        System.out.println("Veuillez indiquer le nombre de températures.");
        int n = in.nextInt(); // the number of temperatures to analyse.
        System.out.println("Il y a "+n+" températures.");
        if (n==0)
        {
            System.out.println("0");
            return;
        }
        ArrayList listTemperaturepos = new ArrayList();
        ArrayList listTemperatureneg = new ArrayList();
        for (int i = 0; i < n; i++) {
            System.out.println("Veuillez indiquer les températures.");
            int t = in.nextInt(); // a temperature expressed as an integer ranging from -273 to 5526
            if (t<0)
            {
                listTemperatureneg.add(t);

            }
            else {
                listTemperaturepos.add(t);
            }
        }
        if (listTemperaturepos.size()>0) {
            Collections.sort(listTemperaturepos);
        }
        Collections.sort(listTemperatureneg, Collections.reverseOrder());
        if (listTemperaturepos.size()>0) {
            int temppos = (int) listTemperaturepos.get(0);
            int tempneg = (int) listTemperatureneg.get(0);

            if (Math.abs(tempneg) < temppos) {
                System.out.println(listTemperatureneg.get(0));
            } else {
                System.out.println(listTemperaturepos.get(0));
            }
        }
        else System.out.println(listTemperatureneg.get(0));

    }

}