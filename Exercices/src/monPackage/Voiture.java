package monPackage;

public class Voiture {
    private int id;
    private String marque;
    private double vitesse;
    private int puissance;

    public Voiture(int id, String marque, double vitesse, int puissance){
        this.id=id;
        this.marque=marque;
        this.vitesse=vitesse;
        this.puissance=puissance;
    }

    public int getId() {
        return id;
    }

    public String getMarque() {
        return marque;
    }

    public double getVitesse() {
        return vitesse;
    }

    public int getPuissance() {
        return puissance;
    }


    public void setMarque(String marque) {
        this.marque = marque;
    }

    public void setVitesse(double vitesse) {
        this.vitesse = vitesse;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    @Override
    public String toString() {
        return "id=" + this.id +
                ", marque='" + marque + '\'' +
                ", vitesse=" + vitesse +
                ", puissance=" + puissance +
                '.';
    }
}
