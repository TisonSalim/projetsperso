package gestionEleves;

public class MainEleve {
    public static void main(String[] args) {

        Eleve eleve1= new Eleve("Sophie");
        eleve1.ajouterNote(12);
        eleve1.ajouterNote(14);
        eleve1.ajouterNote(16);
        Eleve eleve2=new Eleve("Salim");
        eleve2.ajouterNote(22);
        eleve2.ajouterNote(-2);
        eleve2.ajouterNote(16);
        eleve1.ajouterNote(17);
        eleve2.ajouterNote(13);
        System.out.println(eleve1);
        System.out.println(eleve2);

    }
}
