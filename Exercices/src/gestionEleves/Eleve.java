package gestionEleves;

public class Eleve {
    private String nom;
    private int [] listeNotes=null;
    private double moyenne;

    public Eleve (String nom){

        this.nom=nom;
    }

    public String getNom() {
        return nom;
    }

    public int[] getListeNotes() {
        return listeNotes;
    }

    public double getMoyenne() {
        return moyenne;
    }

    public void ajouterNote(int note) {
        if (note < 0) {
            note = 0;
        } else if (note > 20) {
            note = 20;
        }
        if (this.listeNotes == null) {
            this.listeNotes = new int[1];
            this.listeNotes[0] = note;
            this.moyenne = note;
        } else {
            int[] temp = new int[this.listeNotes.length + 1];
            for (int i = 0; i < this.listeNotes.length; i++) {
                temp[i] = this.listeNotes[i];
            }
            temp[temp.length - 1] = note;
            this.listeNotes = temp;
            this.moyenne = ((this.moyenne * (this.listeNotes.length - 1)) + note) / this.listeNotes.length;
        }
    }

    @Override
    public String toString() {
        return "nom='" + nom + '\'' +
                ", moyenne=" + moyenne +
                '.';
    }
}
