package fr.afpa;

public class Specialité {
    private static int id;
    private String code;
    private String libelle;
    public static Professeur[] listeDeProf;

    public Specialité(String libelle){
        this.id=id++;
        this.libelle=libelle;

    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Professeur[] getListeDeProf() { return listeDeProf; }

    public void setListeDeProf(Professeur[] listeDeProf) { this.listeDeProf = listeDeProf; }

    public void Affichelist(){
        
    }

    @Override
    public String toString() {
        return "Specialité { " + libelle +
                " }" + getListeDeProf();
    }
}
