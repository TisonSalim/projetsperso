package fr.afpa;

public class Filiere {
    private static int id;
    private String code;
    private String libelle;

    public Filiere(String libelle){
    this.id=id++;
    this.code=code;
    this.libelle=libelle;
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return "Filiere{" +
                ", libelle='" + libelle + '\'' +
                '}';
    }
}
