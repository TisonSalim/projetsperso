package fr.afpa;

import java.util.Date;

public class Etudiant {
    private static int id;
    private String nom;
    private String prenom;
    private Date dateNaissance;

    public Etudiant(String nom, String prenom, Date dateNaissance){
    this.id=id++;
    this.nom=nom;
    this.prenom=prenom;
    this.dateNaissance=dateNaissance;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @Override
    public String toString() {
        return "Je suis l'étudiant{" +
                "nom='" + nom.substring(0,1).toUpperCase() + nom.substring(1) + '\'' +
                ", prenom='" + prenom.substring(0,1).toUpperCase() +prenom.substring(1) + '\'' +
                ", ma date de naissance est :" + dateNaissance +
                '}';
    }
}
