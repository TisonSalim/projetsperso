package org.sid.dao;

import org.apache.log4j.Logger;
import org.sid.entities.Produit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CatalogueDAOImpl implements ICatalogueDAO {
    private Map<String, Produit> produits = new HashMap<String, Produit>();
    Logger log = Logger.getLogger(this.getClass());

    public void addProduit(Produit p) {
        produits.put(p.getReference(), p);
    }


    public List<Produit> listProduits() {
        return new ArrayList<Produit>(produits.values());
    }

    public Produit getProduit(String ref) {
        return produits.get(ref);
    }

    public void deleteProduit(String ref) {
        produits.remove(ref);
    }

    public void updateProduit(Produit p) {
        produits.put(p.getReference(), p);
    }

    public void initialisation() {
        addProduit(new Produit("RUTI", "POUY", 65492, 5, true));
        addProduit(new Produit("ABCD", "DDDD", 3687, 5, true));
        addProduit(new Produit("LE chat", "avion", 356, 5, true));
        log.info("Initialisation du catalogue produit");
    }

}
