package org.sid.dao;

import org.sid.entities.Produit;

import java.util.List;

public interface ICatalogueDAO {
    public void addProduit(Produit p);
    public List<Produit> listProduits();
    public Produit getProduit(String ref);
    public void deleteProduit(String ref);
    public void updateProduit(Produit p);


}
