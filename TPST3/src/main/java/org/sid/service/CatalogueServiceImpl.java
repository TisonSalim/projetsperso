package org.sid.service;

import org.sid.dao.ICatalogueDAO;
import org.sid.entities.Produit;

import java.util.List;

public class CatalogueServiceImpl implements ICatalogueService {
    private ICatalogueDAO dao;

    public void setDao(ICatalogueDAO dao) {
        this.dao = dao;
    }

    public void addProduit(Produit p) {
        dao.addProduit(p);
    }

    public List<Produit> listProduits() {
        return dao.listProduits();    }


    public Produit getProduit(String ref) {
        return dao.getProduit(ref);
    }

    public void deleteProduit(String ref) {
        dao.deleteProduit(ref);
    }

    public void updateProduit(Produit p) {
        dao.updateProduit(p);
    }
}
