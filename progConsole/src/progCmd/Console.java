package progCmd;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.afpa.progcmd.cmds.Commande;
import fr.afpa.progcmd.cmds.NotFoundCommandeException;

/**
 * @author Laurent HEDON et Julien Delmarle
 *
 *
 */
public class Console {
	public static String repertoireEnCours;
	public static String slash;

	public Console() {
		super();
	}

	/**
	 * @param f Fichier que l'on veut testet
	 * @return boolean true si le fichier envoy� existe
	 */
	public static Boolean fileExists(File f) {
		Pattern monPattern = Pattern.compile("^^\\.|^/\\.|//");
		Matcher monMatcher = monPattern.matcher(f.getName());
		if (monMatcher.find()) {
			System.out.println("Commande non valide");
			return false;
		}
		if (f.exists())
			return true;
		else {
			System.out.println("L'element specifie est introuvable");
			return false;
		}
	}

	/**
	 * @param nomFichier Nom d'un fichier au format String
	 * @return String, le chemin absolu du fichier
	 */
	public static String cheminAbsolu(String nomFichier) {
		if ("..".equals(nomFichier.substring(0, 2))) {
			File f = new File(repertoireEnCours);
			return f.getParent() + nomFichier.substring(2);
		} else if (!":\\".equals(nomFichier.substring(1, 3)) && !"/h".equals(nomFichier.substring(0, 2))) {
			nomFichier = Console.repertoireEnCours + slash + nomFichier;
			return nomFichier;
		} else
			return nomFichier;
	}

	/**
	 * M�thode de d�marrage de la console et de gestion de la saisie utilisateur,
	 * d'appel de cr�ation des commandes
	 */
	public void start() {
		File systeme = new File("C:\\Windows");
		if (systeme.exists()) {
			slash = "\\";
			repertoireEnCours = ("C:\\Users");
		} else {
			slash = "/";
			repertoireEnCours = "/home";
		}
		List<Commande> commandes = new ArrayList<Commande>();

		try {
			commandes.add(Commande.buildCommande("MyHelp"));
		} catch (NotFoundCommandeException e1) {
			System.out.println(e1.getMessage());
		}
		commandes.get(0).execute();

		Scanner scan = new Scanner(System.in);
		System.out.print(repertoireEnCours + ">");
		String saisieClavier = scan.nextLine();
		System.out.println();

		while (!saisieClavier.equals("exit")) {
			String tabSaisieClavier[] = saisieClavier.split(" ");
			ArrayList<String> listSaisie = new ArrayList<String>(Arrays.asList(tabSaisieClavier));
			if (tabSaisieClavier.length == 0) {
				System.out.println(
						"Erreur de saisie, veuillez saisir une commande. Tapez help pour lister les commandes");
			} else {
				String cmd = listSaisie.remove(0);
				if (cmd.length() > 3) {
					cmd = cmd.substring(0, 1).toUpperCase() + cmd.substring(1, 2).toLowerCase()
							+ cmd.substring(2, 3).toUpperCase() + cmd.substring(3).toLowerCase();
				}
				try {
					commandes.add(Commande.buildCommande(cmd));
					commandes.get(commandes.size() - 1).parseParameters(listSaisie);
					commandes.get(commandes.size() - 1).execute();
				} catch (NotFoundCommandeException e) {
					System.out.println(e.getMessage());
					commandes.get(0).execute();
				} catch (IndexOutOfBoundsException e) {
					System.out.println("Fichier specifie absent, verifiez votre saisie");
				}

			}
			System.out.print(repertoireEnCours + ">");
			saisieClavier = scan.nextLine();
			System.out.println();
		}
		System.out.println("Au revoir et à bientôt !");
		scan.close();
		System.exit(0);
	}
}
