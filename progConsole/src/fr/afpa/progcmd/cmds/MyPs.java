package fr.afpa.progcmd.cmds;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import progCmd.Console;

/**
 * Objet commande �quivalent � ps dans la console (affiche les processus en
 * cours, valable sur linux seulement)
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class MyPs extends Commande {

	public MyPs() {
		super();
		this.setName(this.getClass().getName());
	}

	public MyPs(String description) {
		super();
		this.setName(this.getClass().getName());
		this.setDescription(description);
	}

	/**
	 * Execute la commande �quivalente � ps dans un terminal (affiche les processus
	 * en cours, valable sur linux seulement)
	 *
	 */
	@Override
	public boolean execute() {
		File dossierProc = new File("/proc");
		String tabProc[] = dossierProc.list();
		String cmd = "";
		if ("\\".equals(Console.slash)) {
			System.out.println("Commande non valable sur Windows, merci de passer sur Linux");
		} else {
			System.out.println("PID\tCMD");

			for (String string : tabProc) {
				File fichier = new File("/proc/" + string);
				if (fichier.isDirectory() && string.matches("\\d+")) {
					cmd = contenuFichier(new File("/proc/" + string + "/cmdline"));
					System.out.print(string + "\t" + cmd);
				}

			}

		}
		System.out.println();
		return false;
	}

	/**
	 * Enregistrement des param�tres pass�s en arguments
	 */
	@Override
	public void parseParameters(List<String> paramsString) {

	}

	/**
	 * Renvoie le contenu d'un fichier
	 * 
	 * @param f File un fichier
	 * @return String le contenu du fichier
	 */
	public static String contenuFichier(File f) {
		BufferedInputStream bis = null;
		String contenu = "";
		try {
			bis = new BufferedInputStream(new FileInputStream(f));
			byte[] buf = new byte[60];
			int n = 0;
			while ((n = bis.read(buf)) >= 0) {
				for (byte bit : buf) {
					contenu += (char) bit;
				}
				buf = new byte[60];
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println();
			try {
				if (bis != null) {
					bis.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return contenu;
	}

}
