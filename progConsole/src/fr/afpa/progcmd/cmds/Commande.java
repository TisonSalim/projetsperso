package fr.afpa.progcmd.cmds;

import java.util.List;

/**
 * SuperClasse mod�le d'une commande
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public abstract class Commande {
	private String name;
	private String description;
	private Parameters params;

	public Commande() {
		super();

	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Commande(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * @return boolean en fonction de la r�ussite de l'ex�cution
	 */
	public abstract boolean execute();

	public Parameters getParams() {
		return params;
	}

	public void setParams(Parameters params) {
		this.params = params;
	}

	/**
	 * @param String commande le nom de la commande
	 * @return un nouvel objet au nom du param�tre envoy� si la commande existe
	 * @throws NotFoundCommandeException
	 */
	public static Commande buildCommande(String commande) throws NotFoundCommandeException {

		@SuppressWarnings("unchecked")
		Class<Commande> clazzCommande;
		try {
			clazzCommande = (Class<Commande>) Class.forName("fr.afpa.progcmd.cmds." + commande);
			return clazzCommande.newInstance();
		} catch (Exception e) {

			throw new NotFoundCommandeException("La commande <<" + commande + ">> est introuvable");

		}

		/*
		 * if (MyPs.class.getSimpleName().equalsIgnoreCase(commande.toUpperCase())) {
		 * return new MyPs(); }
		 * 
		 * if (MyCd.class.getSimpleName().equalsIgnoreCase(commande.toUpperCase())) {
		 * return new MyCd(); }
		 * 
		 * if (MyRm.class.getSimpleName().equalsIgnoreCase(commande.toUpperCase())) {
		 * return new MyRm(); }
		 * 
		 * if (MyPwd.class.getSimpleName().equalsIgnoreCase(commande.toUpperCase())) {
		 * return new MyPwd(); }
		 */

		// return null;
	}

	/**
	 * @param paramsString Liste de param�tres
	 */
	public abstract void parseParameters(List<String> paramsString);
}
