package fr.afpa.progcmd.cmds;

/**
 * Classe d'exception si une commande ne peut �tre cr��e
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class NotFoundCommandeException extends Exception {

	private static final long serialVersionUID = -3181042612557755202L;

	public NotFoundCommandeException() {
	}

	public NotFoundCommandeException(String arg0) {
		super(arg0);
	}

	public NotFoundCommandeException(Throwable arg0) {
		super(arg0);
	}

	public NotFoundCommandeException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public NotFoundCommandeException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
