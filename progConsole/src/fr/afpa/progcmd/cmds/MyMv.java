package fr.afpa.progcmd.cmds;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import progCmd.Console;

/**
 * Objet commande �quivalent � mv dans la console (d�place un fichier)
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class MyMv extends Commande {
	public MyMv() {
		super();
		this.setName(this.getClass().getName());
	}

	public MyMv(String description) {
		super();
		this.setName(this.getClass().getName());
		this.setDescription(description);
	}

	/**
	 * Execute la commande �quivalente � mv dans la console (d�place un fichier)
	 *
	 */
	@Override
	public boolean execute() {
		InputStream is = null;
		OutputStream os = null;
		File source = new File(Console.cheminAbsolu(this.getParams().getValues().get(0)));
		File destination = new File(Console.cheminAbsolu(this.getParams().getValues().get(1)));

		if (destination.isDirectory()) {
			destination = new File(destination.getAbsolutePath() + Console.slash + source.getName());
		}
		if (source.isDirectory()) {
			System.out.println("Il n'est pas possible de deplacer un repertoire");
		} else if (Console.fileExists(source)) {

			try {
				is = new FileInputStream(source);
				os = new FileOutputStream(destination);
				byte[] buffer = new byte[1024];
				int length;
				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				try {
					is.close();
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if (source.delete()) {
					System.out.println("File moved successfully");
				} else {
					System.out.println("File NOT moved successfully");
				}
			}
		}
		System.out.println();
		return false;
	}

	/**
	 * Enregistrement des param�tres pass�s en arguments
	 */
	@Override
	public void parseParameters(List<String> paramsString) {
		Parameters parameters = new Parameters();
		parameters.setValues(paramsString);
		this.setParams(parameters);
	}

}