package fr.afpa.progcmd.cmds;

import java.io.File;
import java.util.List;

import progCmd.Console;

/**
 * Objet commande �quivalent � rm dans la console (supprime un ou plusieurs
 * fichiers)
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class MyRm extends Commande {
	public MyRm() {
		super();
		this.setName(this.getClass().getName());
	}

	public MyRm(String description) {
		super();
		this.setName(this.getClass().getName());
		this.setDescription(description);
	}

	/**
	 * Execute la commande �quivalente � rm dans un terminal (supprime un ou
	 * plusieurs fichiers)
	 *
	 */
	@Override
	public boolean execute() {
		if (this.getParams().getValues().size() == 0) {
			System.out.println("Merci de spécifier un ou plusieurs fichiers a supprimer\n");
		}
		for (int i = 0; i < this.getParams().getValues().size(); i++) {
			File fileRemove = new File(Console.cheminAbsolu(this.getParams().getValues().get(i)));

			if (fileRemove.delete()) {
				System.out.println("File deleted successfully\n");
			} else {
				System.out.println("L'element specifie est un dossier (non supprimable) ou est introuvable\n");
			}
		}
		return false;
	}

	/**
	 * Enregistrement des param�tres pass�s en arguments
	 */
	@Override
	public void parseParameters(List<String> paramsString) {
		Parameters parameters = new Parameters();
		parameters.setValues(paramsString);
		this.setParams(parameters);
	}

}
