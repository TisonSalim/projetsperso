package fr.afpa.progcmd.cmds;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import progCmd.Console;

/**
 * Objet commande qui affiche une fichier dans la console
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class MyDisplay extends Commande {
	public MyDisplay() {
		super();
		this.setName(this.getClass().getName());
	}

	public MyDisplay(String description) {
		super();
		this.setName(this.getClass().getName());
		this.setDescription(description);
	}

	/**
	 * Affiche le contenu du fichier
	 *
	 */
	@Override
	public boolean execute() throws IndexOutOfBoundsException {

		File f = new File(Console.cheminAbsolu(this.getParams().getValues().get(0)));

		BufferedInputStream bis = null;
		if (f.isDirectory()) {
			System.out.println("Impossible d'afficher un dossier");
		} else {

			try {
				bis = new BufferedInputStream(new FileInputStream(f));
				byte[] buf = new byte[60];
				int n = 0;
				while ((n = bis.read(buf)) >= 0) {
					for (byte bit : buf) {
						System.out.print((char) bit);
					}
					System.out.println();
					buf = new byte[60];
				}
			} catch (FileNotFoundException e) {
				System.out.println("Fichier inexistant, vérifiez votre saisie");
			} catch (IOException e) {
				System.out.println("Impossible de lire l'element specifie");
			} finally {
				System.out.println();
				try {
					if (bis != null) {
						bis.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println();

		return false;
	}

	/**
	 * Enregistrement des param�tres pass�s en arguments
	 */
	@Override
	public void parseParameters(List<String> paramsString) {
		Parameters parameters = new Parameters();
		List<String> values = new ArrayList<String>();
		values.add(paramsString.get(0));
		parameters.setValues(values);
		this.setParams(parameters);
	}

}
