package fr.afpa.progcmd.cmds;

import java.util.List;

import progCmd.Console;

/**
 * Objet commande �quivalent � pwd dans un terminal (affiche le chemin du
 * r�pertoire en cours)
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class MyPwd extends Commande {
	public MyPwd() {
		super();
		this.setName(this.getClass().getName());
	}

	public MyPwd(String description) {
		super();
		this.setName(this.getClass().getName());
		this.setDescription(description);
	}

	/**
	 * Execute la commande �quivalente � pwd dans un terminal (affiche le chemin du
	 * r�pertoire en cours)
	 *
	 */
	@Override
	public boolean execute() {
		return myPwd();
	}

	/**
	 * Execute la commande �quivalente � cd dans un terminal (changement de
	 * r�pertoire)
	 *
	 */
	@Override
	public void parseParameters(List<String> pramsString) {

	}

	/**
	 * Affiche le repertoire en cours
	 * 
	 * @return boolean si l'affichage est r�ussi
	 */
	private boolean myPwd() {

		System.out.println("File destination : " + Console.repertoireEnCours);
		System.out.println();

		return true;
	}

}
