package fr.afpa.progcmd.cmds;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import progCmd.Console;

/**
 * Objet commande �quivalent � cd dans la console (changement de r�pertoire)
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class MyCd extends Commande {
	public MyCd() {
		super();
		this.setName(this.getClass().getName());
	}

	public MyCd(String description) {
		super();
		this.setName(this.getClass().getName());
		this.setDescription(description);
	}

	/**
	 * Execute la commande �quivalente � cd dans un terminal (changement de
	 * r�pertoire)
	 *
	 */
	@Override
	public boolean execute() {
		if ("..".equals(this.getParams().getValues().get(0))) {
			File f = new File(Console.repertoireEnCours);
			Console.repertoireEnCours = f.getParent();
		} else {
			String fichier = Console.cheminAbsolu(this.getParams().getValues().get(0));
			File f = new File(fichier);
			if (Console.fileExists(f)) {
				Console.repertoireEnCours = fichier;
			}
		}
		return false;
	}

	/**
	 * Enregistrement des param�tres pass�s en arguments
	 */
	@Override
	public void parseParameters(List<String> paramsString) {
		Parameters parameters = new Parameters();
		List<String> values = new ArrayList<String>();
		values.add(paramsString.get(0));
		parameters.setValues(values);
		this.setParams(parameters);

	}

}
