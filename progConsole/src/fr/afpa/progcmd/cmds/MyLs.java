package fr.afpa.progcmd.cmds;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;

import progCmd.Console;

/**
 * Objet commande �quivalent � ls dans la console (Affiche le contenu du
 * r�pertoire en cours)
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class MyLs extends Commande {
	public MyLs() {
		super();
		this.setName(this.getClass().getName());
	}

	public MyLs(String description) {
		super();
		this.setName(this.getClass().getName());
		this.setDescription(description);
	}

	/**
	 * Execute la commande �quivalente � � ls dans la console (Affiche le contenu du
	 * r�pertoire en cours)
	 *
	 */
	@Override
	public boolean execute() {
		try {
			if ("-l".equals(this.getParams().getOption())) {
				File f = new File(Console.repertoireEnCours);
				File[] files = f.listFiles();
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				System.out.printf("%-15s %-43s %-30s %10s\n", "TYPE", "NOM", "DERNIERE MODIFICATION", "TAILLE");
				System.out.println(
						"======================================================================================================");
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory())
						System.out.print("Dossier\t\t");
					else
						System.out.print("Ficher\t\t");
					System.out.printf("%-45s %-25s %10d ko", files[i].getName(),
							dateFormat.format(files[i].lastModified()), files[i].length());
					System.out.println();

				}
			} else {
				System.out.println("L'option est incorrecte, seul -l est une option valide");
			}
		} catch (Exception e) {
			File f = new File(Console.repertoireEnCours);
			System.out.println(f);
			for (String ligneDeTableau : f.list()) {
				System.out.println(ligneDeTableau);
			}
		} finally {
			System.out.println();
		}

		return false;
	}

	/**
	 * Enregistrement des param�tres pass�s en arguments
	 */
	@Override
	public void parseParameters(List<String> paramsString) {
		Parameters parameters = null;
		if (paramsString.size() > 0) {
			parameters = new Parameters(paramsString.get(0));
		}
		this.setParams(parameters);
	}

}
