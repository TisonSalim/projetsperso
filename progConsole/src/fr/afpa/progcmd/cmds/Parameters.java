package fr.afpa.progcmd.cmds;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe paramètres qui contient les paramètres des commandes
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class Parameters {

	private String option;
	private List<String> values = new ArrayList<String>();

	public Parameters() {
		super();
	}

	/**
	 * @param option String une option (-R ou -l par exemple)
	 * @param values List une liste de valeurs au format String (generalement le nom
	 *               d'un fichier)
	 */
	public Parameters(String option, List<String> values) {
		super();
		this.option = option;
		this.values = values;
	}

	public Parameters(String option) {
		super();
		this.option = option;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

}
