package fr.afpa.progcmd.cmds;

import java.io.File;
import java.util.List;

import progCmd.Console;

/**
 * Objet commande �quivalent � find dans la console (recherche un fichier)
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class MyFind extends Commande {
	public MyFind() {
		super();
		this.setName(this.getClass().getName());
	}

	public MyFind(String description) {
		super();
		this.setName(this.getClass().getName());
		this.setDescription(description);
	}

	/**
	 * Execute la commande �quivalente � find dans un terminal (recherche de
	 * fichier)
	 *
	 */
	@Override
	public boolean execute() {
		if ("-R".equals(this.getParams().getOption())) {
			myFind(this.getParams().getValues().get(0), Console.repertoireEnCours);
		} else {
			myFind(this.getParams().getValues().get(0));
		}

		System.out.println();
		return false;
	}

	/**
	 * Enregistrement des param�tres pass�s en arguments
	 */
	@Override
	public void parseParameters(List<String> paramsString) {
		Parameters parameters = new Parameters();
		if ("-R".equals(paramsString.get(0))) {
			parameters.setOption("-R");
			paramsString.remove(0);
		} else {
			parameters.setOption("");
		}
		parameters.setValues(paramsString);
		this.setParams(parameters);
	}

	/**
	 * Affiche le fichier recherch� si trouv�
	 * 
	 * @param nomFichier le fichier recherch�
	 */
	public static void myFind(String nomFichier) {
		File repEnCours = new File(Console.repertoireEnCours);
		String tabNomFichiers[] = repEnCours.list();
		Boolean found = false;
		if (tabNomFichiers != null) {
			for (String fichier : tabNomFichiers) {
				if (fichier.toLowerCase().matches(transformeEnRegex(nomFichier.toLowerCase()))) {
					System.out.println(repEnCours + Console.slash + fichier);
					found = true;
				}

			}

		}
		if (!found) {
			System.out.println("Aucun fichier trouve portant ce nom");
		}

	}

	/**
	 * Affiche le fichier recherch� si trouv�, recherche dans les sous-r�pertoires
	 * 
	 * @param nomFichier le fichier recherch�
	 */
	public static void myFind(String nomFichier, String rep) {
		File repEnCours = new File(rep);
		String tabNomFichiers[] = repEnCours.list();
		if (tabNomFichiers != null) {
			for (String fichier : tabNomFichiers) {
				File fichTab = new File(rep + Console.slash + fichier);
				if (fichTab.isDirectory()) {
					myFind(nomFichier, fichTab.getPath());
				}
				if (fichier.toLowerCase().matches(transformeEnRegex(nomFichier.toLowerCase()))) {
					System.out.println(rep + Console.slash + fichier);
				}

			}

		}

	}

	public static String transformeEnRegex(String s) {
		String sRegex = "";
		if (s.length() > 0) {
			for (int i = 0; i < s.length(); i++) {
				switch (s.charAt(i)) {
				case '*':
					sRegex = sRegex + ".*";
					break;
				case '.':
					sRegex += "\\.";
					break;
				case '-':
					sRegex += "\\-";
					break;
				case '$':
					sRegex += "\\$";
					break;
				default:
					sRegex += s.charAt(i);
					break;
				}

			}
		}
		return sRegex;
	}
}
