package fr.afpa.progcmd.cmds;

import java.util.List;

/**
 * Commande qui affiche le menu d'aide
 * 
 * @author Laurent HEDON et Julien Delmarle
 *
 */
public class MyHelp extends Commande {
	public MyHelp() {
		super();
		this.setName(this.getClass().getName());
	}

	public MyHelp(String description) {
		super();
		this.setName(this.getClass().getName());
		this.setDescription(description);
	}

	/**
	 * Affichage du menu d'aide
	 *
	 */
	@Override
	public boolean execute() {

		System.out.println("---------- Help menu ----------\r\n" + "mypwd\t\tAffiche le repertoire en cours\r\n"
				+ "myls\t\tAffiche la liste des fichiers ou des dossiers du repectoire en cours (-l avec detail)\r\n"
				+ "mydisplay\tAffiche le contenu d'un fichier\r\n"
				+ "mycd\t\tSe deplace dans le dossier specifie (.. retour au dossier parent)\r\n"
				+ "myrm\t\tEfface un ou plusieurs fichiers\r\n" + "mycp\t\tCopie le fichier a l'endroit specifie\r\n"
				+ "mymv\t\tDeplace le fichier a l'endroit specifie\r\n"
				+ "myps\t\tAffiche les processus en cours (dispo sur Linux seulement)\r\n"
				+ "myfind\t\tCherche le fichier dans le repertoire en cours (-R sous dossiers compris)\r\n"
				+ "myhelp\t\tAffichage des commandes disponibles\r\n" + "exit\t\tQuitte le programme\r\n");

		return false;
	}

	/**
	 * Enregistrement des param�tres pass�s en arguments
	 */
	@Override
	public void parseParameters(List<String> paramsString) {

	}

}
