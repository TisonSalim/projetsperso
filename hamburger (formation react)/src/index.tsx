import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App";
import Favicon from "react-favicon";


ReactDOM.render(
    <App/>
,
    document.getElementById('root')
);
