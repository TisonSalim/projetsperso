import React, { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';




const PageNotFound: FunctionComponent = () => {

    return (
        <div className="center ">
            <img src="../assets/hamburgers/logo.svg" alt="Page non trouvée"/>
            <h1 className="darkgray">La page recherchée n'existe pas !</h1>
            <Link  to="/" className="waves-effect waves-teal btn-flat darkgray">
                Retourner à l'accueil
            </Link>
        </div>
    );
}

export default PageNotFound;
