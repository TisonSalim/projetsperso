import {FunctionComponent, useState} from "react";
import Hamburger from "../models/hamburger";
import HamburgerForm from "../component/hamburger-form";
import React from "react";

const HamburgerAdd: FunctionComponent = () => {
    const [id] = useState<number>(new Date().getTime());
    const [hamburger] = useState<Hamburger>(new Hamburger(id));

    return (
        <div className="row">
            <h2 className="header center">Ajouter un hamburger</h2>
            <HamburgerForm hamburger={hamburger} isEditForm={false}></HamburgerForm>
        </div>
    );
}

export default HamburgerAdd;