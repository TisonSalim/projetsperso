import React, { FunctionComponent, useState, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Hamburger from '../models/hamburger';
import HAMBURGERS from "../models/mock-hamburger";
import HamburgerForm from "../component/hamburger-form";
import HamburgerService from "../services/hamburger-service";
import Loader from "../component/loader";

type Params = { id: string };

const HamburgerEdit: FunctionComponent<RouteComponentProps<Params>> = ({ match }) => {

    const [hamburger, setHamburger] = useState<Hamburger|null>(null);

    useEffect(() => {
        HamburgerService.getHamburger(+match.params.id).then(hamburger => setHamburger (hamburger));
    }, [match.params.id]);

    return (
        <div>
            { hamburger ? (
                    <div className="row">
                    <h2 className="header center">Compose ton hamburger { hamburger.name }</h2>
                    <HamburgerForm hamburger={hamburger} isEditForm={true}/>
                </div>
    ) : (
        <h4 className="center"><Loader/></h4>
)}
    </div>
);
}

export default HamburgerEdit;
