import React, {FunctionComponent, useState, useEffect} from 'react';
import Hamburger from "../models/hamburger";
import HAMBURGERS from "../models/mock-hamburger";
import HamburgerCard from "../component/hamburger-card";
import HamburgerService from "../services/hamburger-service";
import {Link} from "react-router-dom";
import HamburgerSearch from "../component/hamburger-search";

const HamburgerList: FunctionComponent = () => {
    const [hamburgers, setHamburgers] = useState<Hamburger[]>([]);

    useEffect(() => {
     HamburgerService.getHamburgers().then(hamburgers => setHamburgers(hamburgers))
    }, []);

    return (
        <div>
            <div className="center h-25" ><img src="./assets/hamburgers/logo.svg" alt="logo"/></div>
            <div className="container h-75">
                <HamburgerSearch/>
                <div className="row">
                    {hamburgers.map(hamburger => (
                        <HamburgerCard
                            key={hamburger.id}
                            hamburger={hamburger}
                        />
                    ))}
                </div>
                <Link className="btn-floating btn-large waves-effect waves-light red z-depth-3"
                      style={{position: 'fixed', bottom: '25px', right: '25px'}}
                      to="/hamburger/add">
                    <i className="material-icons">add</i>
                </Link>
            </div>
        </div>
    );
};

export default HamburgerList;
