import React, { FunctionComponent, useState, useEffect } from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import Hamburger from "../models/hamburger";
import formatIngredient from "../helpers/format-ingredient";
import HamburgerService from "../services/hamburger-service";
import Loader from "../component/loader";



type Params = { id: string };

const HamburgerDetail: FunctionComponent<RouteComponentProps<Params>> = ({ match }) => {

    const [hamburger, setHamburger] = useState<Hamburger|null>(null);

    useEffect(() => {
    HamburgerService.getHamburger(+match.params.id).then(hamburger => setHamburger (hamburger));
    }, [match.params.id]);

    return (
        <div>
            { hamburger ? (
                <div className="row">
                    <div className="col s12 m8 offset-m2">
                        <h2 className="header center">{ hamburger.name }</h2>
                        <div className="card hoverable">
                            <div className="card-image">
                                <img src={hamburger.picture} alt={hamburger.name} style={{width: '250px', margin: '0 auto'}}/>
                                <Link  to={`/hamburgers/edit/${hamburger.id}`} className="btn btn-floating halfway-fab waves-effect waves-light">
                                    <i className="material-icons">edit</i>
                                </Link>
                            </div>
                            <div className="card-stacked">
                                <div className="card-content">
                                    <table className="bordered striped">
                                        <tbody>
                                        <tr>
                                            <td>Nom</td>
                                            <td><strong>{ hamburger.name }</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Epices</td>
                                            <td><strong>{ hamburger.epices }</strong></td>
                                        </tr>
                                            <td>Ingrédients</td>
                                            <td>
                                                {hamburger.ingredients.map(ingredient => (
                                                    <span key={ingredient} className={formatIngredient(ingredient)}>{ingredient}</span>
                                                ))}</td>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="card-action">
                                    <Link to="/" >Retour</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ) : (
                <h4 className="center"><Loader/></h4>
            )}
        </div>

    );
}

export default HamburgerDetail;
