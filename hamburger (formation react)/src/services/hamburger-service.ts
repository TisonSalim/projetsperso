import Hamburger from "../models/hamburger";
import HAMBURGERS from "../models/mock-hamburger";


export default class HamburgerService {

    static hamburgers:Hamburger[] = HAMBURGERS;

    static isDev = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development');

    static getHamburgers(): Promise<Hamburger[]> {
        if(this.isDev) {
            return fetch('http://localhost:3001/hamburgers')
                .then(response => response.json())
                .catch(error => this.handleError(error));
        }

        return new Promise(resolve => {
            resolve(this.hamburgers);
        });
    }

    static getHamburger(id: number): Promise<Hamburger|null> {
        if(this.isDev) {
            return fetch(`http://localhost:3001/hamburger/${id}`)
                .then(response => response.json())
                .then(data => this.isEmpty(data) ? null : data)
                .catch(error => this.handleError(error));
        }

        return new Promise(resolve => {
            resolve(this.hamburgers.find(hamburger => hamburger.id === id));
        });
    }

    static updateHamburger(hamburger: Hamburger): Promise<Hamburger> {
        if(this.isDev) {
            return fetch(`http://localhost:3001/hamburgers/${hamburger.id}`, {
                method: 'PUT',
                body: JSON.stringify(hamburger),
                headers: { 'Content-Type': 'application/json'}
            })
                .then(response => response.json())
                .catch(error => this.handleError(error));
        }

        return new Promise(resolve => {
            const { id } = hamburger;
            const index = this.hamburgers.findIndex(hamburger => hamburger.id === id);
            this.hamburgers[index] = hamburger;
            resolve(hamburger);
        });
    }

    static deleteHamburger(hamburger: Hamburger): Promise<{}> {
        if(this.isDev) {
            return fetch(`http://localhost:3001/pokemons/${hamburger.id}`, {
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json'}
            })
                .then(response => response.json())
                .catch(error => this.handleError(error));
        }

        return new Promise(resolve => {
            const { id } = hamburger;
            this.hamburgers = this.hamburgers.filter(hamburger => hamburger.id !== id);
            resolve({});
        });
    }

    static addHamburger(hamburger: Hamburger): Promise<Hamburger> {

        if(this.isDev) {
            return fetch(`http://localhost:3001/pokemons`, {
                method: 'POST',
                body: JSON.stringify(hamburger),
                headers: { 'Content-Type': 'application/json'}
            })
                .then(response => response.json())
                .catch(error => this.handleError(error));
        }

        return new Promise(resolve => {
            this.hamburgers.push(hamburger);
            resolve(hamburger);
        });
    }

    static searchHamburger(term: string): Promise<Hamburger[]> {
        if(this.isDev) {
            return fetch(`http://localhost:3001/hamburgers?q=${term}`)
                .then(response => response.json())
                .catch(error => this.handleError(error));
        }

        return new Promise(resolve => {
            const results = this.hamburgers.filter(hamburger => hamburger.name.includes(term));
            resolve(results);
        });

    }

    static isEmpty(data: Object): boolean {
        return Object.keys(data).length === 0;
    }

    static handleError(error: Error): void {
        console.error(error);
    }
}