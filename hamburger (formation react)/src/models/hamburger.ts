export default class Hamburger {
    // 1. Typage des propiétés d'un Hamburger.
    id: number;
    name: string;
    picture: string;
    epices: string
    ingredients: Array<string>;

    // 2. Définition des valeurs par défaut des propriétés d'un Hamburger.
    constructor(
        id: number,
        name: string = '...',
        picture: string = "/assets/hamburgers/XXX.png ou .jpg",
        epices: string = '',
        ingredients: Array<string> = ['Normal']
    ) {
        // 3. Initialisation des propiétés d'un Hamburger.
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.epices = epices;
        this.ingredients = ingredients;
    }
}
