import Hamburger from "./hamburger";

export const HAMBURGERS: Hamburger[] = [
    {
        id: 1,
        name: "Fabrik",
        picture: "../assets/hamburgers/fabrik.jpg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce barbecue", "Cheddar"]
    },
    {
        id: 2,
        name: "Classique",
        picture: "../assets/hamburgers/classique.jpg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce toscane", "Cheddar"]
    },
    {
        id: 3,
        name: "Mexicain",
        picture: "../assets/hamburgers/mexicain.jpg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce hamburger"]
    },
    {
        id: 4,
        name: "Chti",
        picture: "../assets/hamburgers/chti.jpg",
        epices: "non épicé",
        ingredients: ["Steak", "Sauce maroilles"]
    },
    {
        id: 5,
        name: "Spicy",
        picture: "../assets/hamburgers/spicy.jpg",
        epices: "épicé piquant",

        ingredients: ["Escalope de poulet", "Sauce maison piquante ", "Cheddar"],
    },
    {
        id: 6,
        name: "Végé",
        picture: "../assets/hamburgers/vege.jpg",
        epices: "non épicé",
        ingredients: ["Galette de pomme de terre", "Sauce tartare"]
    },
    {
        id: 7,
        name: "suprême",
        picture: "../assets/hamburgers/supreme.jpg",
        epices: "épicé non piquant",
        ingredients: ["Escalope de poulet", "Sauce tartare", "Cheddar"],
    }
];

export default HAMBURGERS;
