import React, { FunctionComponent, useState } from 'react';
import Hamburger from "../models/hamburger";
import "./hamburger-card.css";
import formatIngredient from "../helpers/format-ingredient";
import {useHistory} from 'react-router-dom';

type Props = {
    hamburger: Hamburger;
    borderColor?: string;
};

const HamburgerCard: FunctionComponent<Props> = ({
    hamburger,
    borderColor = "#a0a3aa"
}) => {
    const [color, setColor] = useState<string>("");
    const history = useHistory();

    const showBorder = () => {
        setColor(borderColor);
    }

    const hideBorder = () => {
        setColor("#f5f5f5");
    }

    const goToHamburger = (id: number) => {
        history.push('/hamburgers/'+id);
    }

    return (
        <div
            className="col s6 m4" onClick={() => goToHamburger(hamburger.id)}
            onMouseEnter={showBorder}
            onMouseLeave={hideBorder}
        >

            <div
                className="card horizontal mouseEnter"
                style={{ borderColor: color}}
            >
                <div className="card-image">
                    <img height="100%" width="100%"src={hamburger.picture} alt={hamburger.name} />
                </div>

                <div className="card-stacked">
                    <div className="card-content">
                        <p className="darkgray">{hamburger.name}</p>
                        {hamburger.ingredients.map(ingredient =>
                        <span key={ingredient} className={formatIngredient(ingredient)}>{ingredient}</span>)}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HamburgerCard;
