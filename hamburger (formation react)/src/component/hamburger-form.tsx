import React, {FunctionComponent, useState} from "react";
import {useHistory} from "react-router-dom";
import formatType from "../helpers/format-ingredient";
import Hamburger from "../models/hamburger";
import HamburgerService from "../services/hamburger-service";

type Props = {
    hamburger: Hamburger;
    isEditForm: boolean;
};

type Field = {
    value?: any;
    error?: string;
    isValid?: boolean;
};

type Form = {
    picture: Field;
    name: Field;
    epices: Field;
    ingredients: Field;
};

const HamburgerForm: FunctionComponent<Props> = ({hamburger, isEditForm}) => {
    const ingredients: string[] = [
        "Salade",
        "Steak",
        "Tomate",
        "Galette de pomme de terre",
        "Oignon",
        "Poulet",
        "Poivron",
        "Bacon",
        "Fromage",
        "Aubergine",
        "Cheddar",
        "Sauce barbecue",
        "Sauce toscane",
        "Sauce hamburger",
        "Sauce tartare",
        "Sauce maroilles",
        "Sauce maison piquante",
    ];

    const [form, setForm] = useState<Form>({
        picture: {value: hamburger.picture},
        name: {value: hamburger.name, isValid: true},
        epices: {value: hamburger.epices, isValid: true},
        ingredients: {value: hamburger.ingredients, isValid: true},
    });

    const history = useHistory();

    const hasIngredient = (ingredient: string): boolean => {
        return form.ingredients.value.includes(ingredient);
    };

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const fieldName: string = e.target.name;
        const fieldValue: string = e.target.value;
        const newField: Field = {[fieldName]: {value: fieldValue}};

        setForm({...form, ...newField});
    };

    const selectIngredient = (
        ingredient: string,
        e: React.ChangeEvent<HTMLInputElement>
    ): void => {
        const checked = e.target.checked;
        let newField: Field;

        if (checked) {
            const newIngredients: string[] = form.ingredients.value.concat([
                ingredient,
            ]);
            newField = {value: newIngredients};
        } else {
            const newIngredients: string[] = form.ingredients.value.filter(
                (currentIngredient: string) => currentIngredient !== ingredient
            );
            newField = {value: newIngredients};
        }

        setForm({...form, ...{ingredients: newField}});
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const isFormValid = validateForm();
        if (isFormValid) {
            hamburger.picture = form.picture.value;
            hamburger.name = form.name.value;
            hamburger.ingredients = form.ingredients.value;
            hamburger.epices = form.epices.value;
        }

        isEditForm ? updateHamburger() : addHamburger();
    };

    const isAddForm = () => {
        return !isEditForm;
    };

    const validateForm = () => {
        let newForm: Form = form;

        //Validator URL
        if (isAddForm()) {
            const start = "/assets/hamburgers/";
            const endjpg = ".jpg";
            const endpng = ".png";

            if (
                !form.picture.value.StartsWith(start) &&
                !form.picture.value.endsWith(endjpg || endpng)
            ) {
                const errorMessage: string = "L'url n'est pas valide";
                const newField: Field = {
                    value: form.picture.value,
                    error: errorMessage,
                    isValid: false,
                };
            } else {
                const newField: Field = {value: form.picture.value, error: '', isValid: true};
                newForm = {...form, ...{picture: newField}};
            }
        }

        // Validator name
        if (!/^[a-zA-Zàéè ]{3,25}$/.test(form.name.value)) {
            const errorMsg: string = "Le nom du hamburger est requis (1-25).";
            const newField: Field = {
                value: form.name.value,
                error: errorMsg,
                isValid: false,
            };
            newForm = {...newForm, ...{name: newField}};
        } else {
            const newField: Field = {
                value: form.name.value,
                error: "",
                isValid: true,
            };
            newForm = {...newForm, ...{name: newField}};
        }
        setForm(newForm);
        return newForm.name.isValid;
    };

    const isIngredientsValid = (ingredient: string): boolean => {
        // Cas n°1: L'hamburger a un seul type, qui correspond au type passé en paramètre.
        // Dans ce cas on revoie false, car l'utilisateur ne doit pas pouvoir décoché ce type (sinon l'hamburger aurait 0 type, ce qui est interdit)
        if (form.ingredients.value.length === 1 && hasIngredient(ingredient)) {
            return false;
        }

        // Cas n°1: Le pokémon a au moins 7 ingredients.
        // Dans ce cas il faut empêcher à l'utilisateur de cocher un nouveau type, mais pas de décocher les types existants.
        if (form.ingredients.value.length >= 7 && !hasIngredient(ingredient)) {
            return false;
        }

        // Après avoir passé les deux tests ci-dessus, on renvoie 'true',
        // c'est-à-dire que l'on autorise l'utilisateur à cocher ou décocher un nouveau type.
        return true;
    };

    const addHamburger = () => {
        HamburgerService.addHamburger(hamburger).then(() => {
            history.push(`/hamburger/${hamburger.id}`);
        })
    }

    const updateHamburger = () => {
        HamburgerService.updateHamburger(hamburger).then(() =>
            history.push(`/hamburgers/${hamburger.id}`)
        );
    }

    const deleteHamburger = () => {
        HamburgerService.deleteHamburger(hamburger).then(() =>
            history.push(`/hamburgers`)
        );
    };

    return (
        <form onSubmit={(e) => handleSubmit(e)}>
            <div className="row">
                <div className="col s12 m8 offset-m2">
                    <div className="card hoverable">{
                        isEditForm && (
                            <div className="card-image">
                                <img
                                    src={hamburger.picture}
                                    alt={hamburger.name}
                                    style={{width: "250px", margin: "0 auto"}}
                                />
                                <span className="btn-floating halfway-fab waves-effect waves-light">
                <i onClick={deleteHamburger} className="material-icons">
                  delete
                </i>
              </span>
                            </div>)}
                        <div className="card-stacked">
                            <div className="card-content">{/* Hamburger picture */}
                                {isAddForm() && (
                                    <div className="form-group">
                                        <label htmlFor="picture">Image</label>
                                        <input
                                            id="picture"
                                            name="picture"
                                            type="text"
                                            className="form-control"
                                            value={form.picture.value}
                                            onChange={(e) => handleInputChange(e)}
                                        />
                                        {form.picture.error && (
                                            <div className="card-panel red accent-1">
                                                {form.picture.error}{" "}
                                            </div>
                                        )}
                                    </div>)}

                                {/* Hamburger name */}
                                <div className="form-group">
                                    <label htmlFor="name">Nom</label>
                                    <input
                                        id="name"
                                        name="name"
                                        type="text"
                                        className="form-control"
                                        value={form.name.value}
                                        onChange={(e) => handleInputChange(e)}
                                    />
                                    {form.name.error && (
                                        <div className="card-panel red accent-1">
                                            {form.name.error}{" "}
                                        </div>
                                    )}
                                </div>
                                {/* Hamburger type */}
                                <div className="form-group">
                                    <label htmlFor="epices">Epicée ou non</label>
                                    <input
                                        id="epices"
                                        type="string"
                                        className="form-control"
                                        value={form.epices.value}
                                        name="type"
                                        onChange={(e) => handleInputChange(e)}
                                    />
                                </div>
                                {/* Hamburger ingredients */}
                                <div className="form-group">
                                    <label>Ingrédients</label>
                                    {ingredients.map((ingredient) => (
                                        <div key={ingredient} style={{marginBottom: "10px"}}>
                                            <label>
                                                <input
                                                    id={ingredient}
                                                    type="checkbox"
                                                    className="filled-in"
                                                    value={ingredient}
                                                    checked={hasIngredient(ingredient)}
                                                    name="ingredients"
                                                    onChange={(e) => selectIngredient(ingredient, e)}
                                                    disabled={!isIngredientsValid(ingredient)}
                                                />
                                                <span>
                          <p className={formatType(ingredient)}>{ingredient}</p>
                        </span>
                                            </label>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            <div className="card-action center">
                                {/* Submit button */}
                                <button type="submit" className="btn">
                                    Valider
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    );
};

export default HamburgerForm;
