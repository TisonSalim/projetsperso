import React, { FunctionComponent, useState } from 'react';
import { Link } from 'react-router-dom';
import Pokemon from '../models/hamburger';
import HamburgerService from "../services/hamburger-service";

const HamburgerSearch: FunctionComponent = () => {

    const [term, setTerm] = useState<string>('');
    const [hamburgers, setHamburgers] = useState<Pokemon[]>([]);

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const term = e.target.value;
        setTerm(term);

        if(term.length <= 1) {
            setHamburgers([]);
            return;
        }

        HamburgerService.searchHamburger(term).then(hamburgers => setHamburgers(hamburgers));
    }

    return (
        <div className="row">
            <div className="col s12 m6 offset-m3">
                <div className="card">
                    <div className="card-content">
                        <div className="input-field">
                            <input type="text" placeholder="Rechercher un hamburger" value={term} onChange={e => handleInputChange(e)} />
                        </div>
                        <div className='collection'>
                            {hamburgers.map((hamburger) => (
                                <Link key={hamburger.id} to={`/hamburgers/${hamburger.id}`} className="collection-item">
                                    {hamburger.name}
                                </Link>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HamburgerSearch;