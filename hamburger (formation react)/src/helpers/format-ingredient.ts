const formatIngredient  = (ingredient: string): string => {
    let color: string;

    switch (ingredient) {
        case 'Salade':
            color = 'red lighten-1';
            break;
        case 'Steak':
            color = 'red lighten-1';
            break;
        case 'Tomate':
            color = 'green lighten-1';
            break;
        case 'Galette de pomme de terre':
            color = 'brown lighten-1';
            break;
        case 'Oignon':
            color = 'grey lighten-3';
            break;
        case 'Poulet':
            color = 'blue lighten-3';
            break;
        case 'Poivron':
            color = 'deep-purple accent-1';
            break;
        case 'Bacon':
            color = 'pink lighten-4';
            break;
        case 'Fromage':
            color = 'deep-purple darken-2';
            break;
        case 'Aubergine':
            color = 'lime accent-1';
            break;
        case 'Cheddar':
            color = 'deep-orange';
            break;
        case 'Sauce barbecue':
            color = 'brown lighten-1';
            break;
        case 'Sauce toscane':
            color = 'brown lighten-1';
            break;
        case 'Sauce hamburger':
            color = 'brown lighten-1';
            break;
        case 'Sauce tartare':
            color = 'brown lighten-1';
            break;
        case 'Sauce maroilles':
            color = 'brown lighten-1';
            break;
        case 'Sauce maison piquante':
            color = 'brown lighten-1';
            break;
        default:
            color = 'grey';
            break;
    }

    return `chip ${color}`;

}

export default formatIngredient;
