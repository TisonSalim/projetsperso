import React, {FunctionComponent} from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import HamburgerList from "./pages/hamburger-list";
import HamburgerDetail from "./pages/hamburger-detail";
import PageNotFound from "./pages/page-not-found";
import "./App.css"
import HamburgerEdit from "./pages/hamburger-edit";
import HamburgerAdd from "./pages/hamburger-add";
import Login from "./pages/login";
import PrivateRoute from "./PrivateRoute";


const App: FunctionComponent = () => {

    return (
        <Router>
            <div>
                <nav className="mb-1 navbar navbar-expand-lg navbar-dark default-color">
                    <Link className="navbar-brand" to="/">
                        <img src="/assets/hamburgers/logo.svg" width="68" height="68"
                             alt="" className="d-inline-block align-middle mr-2"/>
                             <p>Ceci est un test</p>
                    </Link>
                </nav>
                <Switch>
                    <PrivateRoute exact path="/" component={HamburgerList}/>
                    <Route exact path="/login" component={Login}/>
                    <PrivateRoute exact path="/hamburgers" component={HamburgerList}/>
                    <PrivateRoute exact path="/hamburger/add" component={HamburgerAdd}/>
                    <PrivateRoute exact path="/hamburgers/edit/:id" component={HamburgerEdit}/>
                    <PrivateRoute path="/hamburgers/:id" component={HamburgerDetail}/>
                    <Route component={PageNotFound}/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;


