package monPackage;
import java.util.ArrayList;
import java.util.List;
public class Spectateur {
    String nom;
    int age;
    String tarif;

    public Spectateur(String n, int a) {
        this.nom = n;
        this.age = a;
        Theatre.listDesSpectateurs.add(this);
    }

    public String toString() {
        return String.format("Le nom du client est %s, il a %d ans. Il paiera donc un %s !", this.nom, this.age, this.getTarif());
    }

    public String getNom() {
        return nom;
    }

    public String getTarif() {
        if (age < 15)
            tarif = "tarif réduit";
        else
            tarif = "plein tarif";
        return tarif;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {
    }
}
