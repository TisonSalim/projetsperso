package monPackage;
import java.util.ArrayList;
import java.util.List;
// on crée l'objet spectacle
public class Spectacle {
    //on initilise les listes 2 listes (différences ? listdesspec est celle qui confond tout)
    List<Spectateur> spectateurs = new ArrayList<>();
    List<Spectateur> listDesSpectateurs = new ArrayList<>();
    String nomDuSpectacle;
    int prixDuSpectacle;
    int nbDePlaces;

    public Spectacle(String nds, int pds) {
        this.nomDuSpectacle = nds;
        this.prixDuSpectacle = pds;
        this.nbDePlaces = 99;
        Theatre.listeSpectacle.add(this);
    }
    //on crée la fonction qui permet d'ajouter des spectateurs
    public void ajouterSpectateur(Spectateur s) {
       //tant que le nb de place est supérieur à 0 on l'introduit dans la liste spectateurs puis dans la plus grande liste.
        if (nbDePlaces > 0) {
            this.spectateurs.add(s);
            listDesSpectateurs.add(s);
            // le nb de place diminue de 1 à chaque fois.
            nbDePlaces--;
        } else {
            System.out.println("Plus de places !");
        }
    }
    //permet de supprimer un spectateur.
    void supprimerSpectateur(Spectateur s) {
        this.spectateurs.remove(s);
    }
    //permet d'afficher des infos en utilisant "this.
    public String toString() {
        return String.format("Le spectacle %s a encore %d places , la place est à %d Euros pour une recette totale de %d Euros pour l'instant.", this.nomDuSpectacle, this.nbDePlaces, this.prixDuSpectacle, this.prixTotal());
    }

    int prixTotal() {
        int prix = 10;
        int sommePrix = 0;
        for (Spectateur spectateur : spectateurs) {
            if (spectateur.age < 15)
                sommePrix = sommePrix + (prix / 2);
            else
                sommePrix = sommePrix + prix;
        }
        return sommePrix;
    }
}
