public class Planete {
    String nom;
    int diametre;
    String matiere;
    int totalVisiteurs;
    Atmosphere atmosphere;
    Vaisseau    vaisseauPresent;
    static String forme="Sphérique";

    int revolution(int degres){
        System.out.println("Je suis la planète "+nom+" et je tourne autour de mon étoile de "+degres+" degrés.");
        return degres/360;
    }

    int rotation(int degres){
        System.out.println("Je suis la planète "+nom+" et je tourne sur moi-même de "+degres+" degrés.");
        return degres/360;
    }

    void TotalVisiteurs(int nbpassagers){

        totalVisiteurs=totalVisiteurs+nbpassagers;
    }

    Vaisseau accueillirVaisseau(Vaisseau vaisseau){
        if (vaisseauPresent==null){
            System.out.println("Aucun vaisseau s'en va !");
        }
        else {
            System.out.println("Un vaisseau doit s'en aller !");

        }

        totalVisiteurs=totalVisiteurs+vaisseau.nbpassagers;
        System.out.println("Le nombre total de passager est "+totalVisiteurs);
        Vaisseau vaisseauPrecedent=vaisseauPresent;
        vaisseauPresent=vaisseau;
        return vaisseauPrecedent;

    }
}